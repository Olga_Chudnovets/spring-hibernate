package com.epam.edu.springhibernate.persistence.service;
/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */

import com.epam.edu.springhibernate.MyPersistenceJPAConfig;
import com.epam.edu.springhibernate.persistence.model.Quest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MyPersistenceJPAConfig.class }, loader = AnnotationConfigContextLoader.class)
public class QuestServiceTest
{

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private IQuestService questService;

	private Session session;

	// tests

	@Before
	public final void before() {
		session = sessionFactory.openSession();
	}

	@After
	public final void after() {
		session.close();
	}

	// tests


	@Test
	public final void whenEntityIsCreated_thenNoExceptions() {
		questService.create(new Quest("Save beautifull princess"));
	}

}

