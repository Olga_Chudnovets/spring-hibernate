package com.epam.edu.springhibernate.persistence.service.impl;

import com.epam.edu.springhibernate.persistence.dao.IKnightDao;
import com.epam.edu.springhibernate.persistence.dao.common.IOperations;
import com.epam.edu.springhibernate.persistence.model.Knight;
import com.epam.edu.springhibernate.persistence.service.IKnightService;
import com.epam.edu.springhibernate.persistence.service.common.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */
public class KnightService extends AbstractService<Knight> implements IKnightService
{

	@Autowired
	private IKnightDao dao;

	public KnightService()
	{
		super();
	}

	// API

	@Override
	protected IOperations<Knight> getDao()
	{
		return dao;
	}

}
