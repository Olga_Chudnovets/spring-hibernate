package com.epam.edu.springhibernate.persistence.service.impl;

import com.epam.edu.springhibernate.persistence.dao.IQuestDao;
import com.epam.edu.springhibernate.persistence.dao.common.IOperations;
import com.epam.edu.springhibernate.persistence.model.Quest;
import com.epam.edu.springhibernate.persistence.service.IQuestService;
import com.epam.edu.springhibernate.persistence.service.common.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */
@Service
public class QuestService extends AbstractService<Quest> implements IQuestService
{

	@Autowired
	private IQuestDao dao;

	public QuestService() {
		super();
	}

	// API

	@Override
	protected IOperations<Quest> getDao() {
		return dao;
	}


}
