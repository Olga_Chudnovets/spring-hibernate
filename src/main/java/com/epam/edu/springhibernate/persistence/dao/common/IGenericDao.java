package com.epam.edu.springhibernate.persistence.dao.common;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */
import java.io.Serializable;

public interface IGenericDao<T extends Serializable> extends IOperations<T> {
	//
}
