package com.epam.edu.springhibernate.persistence.service;

import com.epam.edu.springhibernate.persistence.dao.common.IOperations;
import com.epam.edu.springhibernate.persistence.model.Knight;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */
public interface IKnightService  extends IOperations<Knight>
{
}
