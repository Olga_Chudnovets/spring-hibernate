package com.epam.edu.springhibernate.persistence.dao.impl;

import com.epam.edu.springhibernate.persistence.dao.IQuestDao;
import com.epam.edu.springhibernate.persistence.dao.common.AbstractHibernateDao;
import com.epam.edu.springhibernate.persistence.model.Quest;
import org.springframework.stereotype.Repository;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */
@Repository
public class QuestDao extends AbstractHibernateDao<Quest> implements IQuestDao
{

	public QuestDao()
	{
		super();

		setClazz(Quest.class);
	}

	// API

}
