package com.epam.edu.springhibernate.persistence.model;
import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Quest implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(targetEntity = Knight.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "KNIGHT_ID")
    private Knight knight = new Knight();

    public Quest() {
        super();
    }

    public Quest(final String name) {
        super();
        this.name = name;
    }

    //

    public Knight getKnight() {
        return knight;
    }

    public void setKnight(final Knight knight) {
        this.knight = knight;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    //

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Quest other = (Quest) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Foo [name=").append(name).append("]");
        return builder.toString();
    }

}