package com.epam.edu.springhibernate.persistence.dao.common;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */
import java.io.Serializable;
import java.util.List;
public interface IOperations <T extends Serializable>
{
	T findOne(final long id);

	List<T> findAll();

	void create(final T entity);

	T update(final T entity);

	void delete(final T entity);

	void deleteById(final long entityId);
}
