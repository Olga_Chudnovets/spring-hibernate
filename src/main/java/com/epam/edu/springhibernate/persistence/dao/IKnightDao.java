package com.epam.edu.springhibernate.persistence.dao;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */

import com.epam.edu.springhibernate.persistence.dao.common.IOperations;
import com.epam.edu.springhibernate.persistence.model.Knight;

public interface IKnightDao extends IOperations<Knight> {
	//
}
