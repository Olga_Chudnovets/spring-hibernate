package com.epam.edu.springhibernate.persistence.dao.impl;

import com.epam.edu.springhibernate.persistence.dao.IKnightDao;
import com.epam.edu.springhibernate.persistence.dao.common.AbstractHibernateDao;
import com.epam.edu.springhibernate.persistence.model.Knight;
import org.springframework.stereotype.Repository;

/**
 * Created by Olga_Chudnovets on 10.05.2015.
 */

@Repository
public class KnightDao extends AbstractHibernateDao<Knight> implements IKnightDao
{

	public KnightDao()
	{
		super();

		setClazz(Knight.class);
	}

	// API

}
