package com.epam.edu.springhibernate.controller;

import com.epam.edu.springhibernate.persistence.model.Quest;
import com.epam.edu.springhibernate.persistence.service.IQuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Olga_Chudnovets on 12.05.2015.
 */
@RestController
@RequestMapping("/service/greeting")
public class SpringServiceController
{
	@Autowired
	private IQuestService questService;

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public String getGreeting(@PathVariable String name) {
		questService.create(new Quest("Save beautifull princess22"));
		String result="Hello "+name;
		return result;
	}

}
